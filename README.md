## meta-lemuria yocto layer

```
repo init -u ssh://git@gitlab.com/linumiz/lemuria/meta-lemuria.git -m conf/samples/lemuria.xml -b lemuria && repo sync -j10 && cd lemuria && TEMPLATECONF=meta-lemuria/conf/samples/ source oe-init-build-env && bitbake core-image-base
```
