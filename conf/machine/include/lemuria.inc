PREFERRED_PROVIDER_virtual/kernel = "linux-raspberrypi"
PREFERRED_VERSION_linux-raspberrypi = "5.10.%"
PREFERRED_PROVIDER_u-boot-default-script = "rpi-u-boot-scr"

SOC_FAMILY = "rpi"
include conf/machine/include/soc-family.inc

MACHINE_EXTRA_RRECOMMENDS += "\
    linux-firmware-rpidistro-bcm43455 \
    bluez-firmware-rpidistro-bcm4345c0-hcd \
"
MACHINE_FEATURES += "usbhost vfat ext2 bluetooth wifi sdio"
MACHINE_EXTRA_RRECOMMENDS += "kernel-modules udev-rules-rpi"
MACHINE_ESSENTIAL_EXTRA_RRECOMMENDS += "kernel-module-i2c-dev kernel-module-i2c-bcm2708"
BAD_RECOMMENDATIONS += "udev-hwdb"

UBOOT_ENTRYPOINT = "0x00080000"
UBOOT_LOADADDRESS = "0x00080000"
UBOOT_MACHINE = "rpi_arm64_config"

ARMSTUB ?= "armstub8-gic.bin"

# FIXME compressed kernel image needs to be enabled
FIT_KERNEL_COMP_ALG = "none"
FIT_KERNEL_COMP_ALG_EXTENSION = ""

FITLOADADDR ?= "0x1f000000"
KERNEL_CLASSES += "kernel-fitimage"
KERNEL_IMAGETYPE = "fitImage"
KERNEL_DEVICETREE = "broadcom/bcm2711-rpi-cm4.dtb"

IMAGE_FSTYPES ?= "squashfs-zst wic.bz2"

SERIAL_CONSOLES = "115200;ttyS0"

BOOTFILES_DIR_NAME ?= "bootfiles"

IMAGE_BOOT_FILES ?= "${BOOTFILES_DIR_NAME}/* \
                 bcm2711-rpi-cm4.dtb \
                 ${KERNEL_IMAGETYPE} u-boot.bin;kernel8.img boot.scr', \
                 "
# The kernel image is installed into the FAT32 boot partition and does not need
# to also be installed into the rootfs.
RDEPENDS:${KERNEL_PACKAGE_NAME}-base = ""

do_image_wic[depends] += " \
    rpi-bootfiles:do_deploy \
    u-boot:do_deploy \
    "

do_image_wic[recrdeps] = "do_build"

WKS_FILE ?= "emmc-lemuria-ro.wks"
